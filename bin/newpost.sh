#! /usr/bin/bash

TODAYS_DATE=$(date +%F)
BLOG_PATH=~/dev/blog/_posts/
POST_TITLE=$1
FORMATTED_POST_TITLE=$(echo $POST_TITLE | tr " " "-" | tr "[:upper:]" "[:lower:]" | tr "." "-")
FILENAME="$BLOG_PATH$TODAYS_DATE-$FORMATTED_POST_TITLE.md"
touch $FILENAME
echo -e "---\nlayout: post\ntitle: $1\npermalink: /notes/$FORMATTED_POST_TITLE\n---" > $FILENAME
cd $BLOG_PATH && nvim $FILENAME
