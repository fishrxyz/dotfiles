#!/usr/bin/bash

PROJECTS_PATH="$HOME/git"

dir=$(ls -ld $PROJECTS_PATH/* | sed -r 's/^.+\///' | fzf --reverse --header="Select project from $PROJECTS_PATH >")
session_name=$(echo $dir | tr '.' '_')

session_exists() {
   tmux has-session -t $session 2>/dev/null
}

create_session() {
   project_dir=$PROJECTS_PATH/$dir
   (
      TMUX='' 
      tmux new-session -Ad -s $session_name -c $project_dir;
      tmux send-keys -t "$session_name" "nvim '+Telescope find_files'" Enter;
   )

}

attach_to_existing_session() {
   tmux switch-client -t $session_name
}

if ! session_exists; then
   create_session
   attach_to_existing_session
else
   attach_to_existing_session
fi


