# List active tmux sessions
# Split the output by colon and extract the names
# Pass the output to fzf
# If the result is empty we exit
# else we switch to the chosen session


session_name=$(tmux list-sessions | cut -d ':' -f1 | fzf --reverse)

if [ ! "$session_name" ]; then
   exit 1
else
   tmux switch-client -t "$session_name"
fi
