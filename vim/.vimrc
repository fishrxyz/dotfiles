" Set the shell to bash if using vim
if &shell =~# 'fish$'
    set shell=bash
endif

set background=dark
set binary
set noeol

if &term =~ '256color'
    " Disable Background Color Erase (BCE) so that color schemes
    " work properly when Vim is used inside tmux and GNU screen.
    set t_ut=
endif

set encoding=utf-8              " Necessary to show Unicode glyphs
set showmode                    " always show what mode we're currently editing in
set nocompatible                " be iMproved, required

" Tabs vs Spaces
set tabstop=4                   " a tab is four spaces
set softtabstop=4               " a tab is four spaces when editing
set expandtab                   " <tab> becomes a shortcut for 'insert four spaces'

" Visual Settings
set number                      " show line numbers
set relativenumber              " shows numbers relative to the current line
set showcmd                     " show the very last command in the bottom right
set hlsearch                    " Hightlight words during search
set autoindent                  " always set autoindenting on
set copyindent                  " copy the previous indentation on autoindenting
set smartindent
set showmatch                   " Always show matching parenthesis when one is hightlighted
set lazyredraw                  " redraw only when we need to
set shiftwidth=4                " number of spaces to use for autoindenting
set textwidth=80                " Maximum width of the editor
set colorcolumn=80              " set a colored column to avoid going too far to the right
set list
set listchars=tab:>-


set hidden
set history=100

set splitright                  " create a new split to the right when calling :new
set clipboard+=unnamedplus

" Ignore swp pyc and other python(ish) related files
set wildignore="*.swp, *.pyc, *.o"

" Change terminal title
set title

" Lazy redrawing when executing macros
set lazyredraw
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"


" Fix indentation issues in YAML config files
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType yml  setlocal ts=2 sts=2 sw=2 expandtab

" ** - General vim config - **

set magic " for regex

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile

" Set tags directory
set tags=tags;

" Useful mappings for managing tabs

" map leader key to space
let mapleader=" "

inoremap jk <ESC>

map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove<cr>
map <leader>te :tabedit<cr>

" enable fast saving of files using <space> w
nmap <leader>w :w!<cr>


" write quit easily using <space> q
nmap <leader>q :wq!<cr>

" source .vimrc using <space> s
map <leader>s :source ~/.vimrc<CR>

" toggle NERDTree with <space> n
nmap <leader>n :NERDTreeToggle<CR>
nmap <leader>m :NERDTreeFind<CR>

" Enable folding
set foldmethod=indent
set foldlevel=99

" Cancel a search with escape
map <leader>/ :nohlsearch<CR>

" Prettify json
nmap <leader>js :%! python -m json.tool<CR>

" Enable folding with the spacebar
nnoremap Q <nop>


" Disable Arrow Keys
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
nnoremap j gj
nnoremap k gk

" Easier split navigation

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap , <C-W><C-W>

nnoremap <S-Tab> :tabnext<CR>
nnoremap <S-C-Tab> :tabprevious<CR>

nmap G Gzz
nmap n nzz
nmap N Nzz


"""""""""""""""""""""""""""
"    Plugins List         "
"                         "
"""""""""""""""""""""""""""

" Plugin management using junegunn/vim-plug
call plug#begin('~/.vim/plugged')

" Plugin list

Plug 'tpope/vim-commentary'
Plug 'https://github.com/ryanoasis/vim-devicons'
Plug 'victorze/foo'
Plug 'Yggdroot/indentLine'
Plug 'connorholyday/vim-snazzy'
Plug 'vim-python/python-syntax'
Plug 'mhinz/vim-startify'
Plug 'mxw/vim-jsx'
Plug 'airblade/vim-gitgutter'
Plug 'dracula/vim', {'as': 'dracula'}
Plug 'tpope/vim-surround'
Plug 'sheerun/vim-polyglot'
Plug 'elzr/vim-json'
Plug 'plasticboy/vim-markdown'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'mitsuhiko/vim-jinja', { 'for': 'jinja2' }
Plug 'scrooloose/nerdtree'
Plug 'kien/ctrlp.vim'
Plug 'bling/vim-airline'
Plug 'jiangmiao/auto-pairs'
Plug 'mattn/emmet-vim'
Plug 'nvie/vim-flake8', { 'for': 'python' }
Plug 'mkitt/tabline.vim'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'deoplete-plugins/deoplete-jedi'
Plug 'w0rp/ale'


call plug#end()

syntax on
syntax enable
filetype plugin indent on       " allows filetype detection

if has("nvim")
    set termguicolors
    set cole=0
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
    color dracula
endif

" set indentation settings for python files
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=119 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix |

" Remove whitespace on save
" autocmd BufWritePre * :%s/\s\+$//e

autocmd FileType javascript setlocal shiftwidth=2 tabstop=2
autocmd FileType typescript setlocal shiftwidth=2 tabstop=2

" format javascript using prettier on save
" autocmd FileType javascript set formatprg=prettier\ --stdin


"""""""""""""""""""""""""""
"    Plugins Settings     "
"                         "
"""""""""""""""""""""""""""

" ** - Python settings - **
let python_highlight_all=1
let g:pymode_python='python3'


" Indentline settings
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

" ** - Markdown shizzle - **

" disable header folding
let g:vim_markdown_folding_disabled = 1

" do not use conceal feature, the implementation is not so good
let g:vim_markdown_conceal = 0
let g:vim_markdown_conceal_code_blocks = 0

" disable math tex conceal feature
let g:tex_conceal = ""
let g:vim_markdown_math = 1

" support front matter of various format
let g:vim_markdown_frontmatter = 1  " for YAML format
let g:vim_markdown_toml_frontmatter = 1  " for TOML format
let g:vim_markdown_json_frontmatter = 1  " for JSON format

autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!

" Error and warning signs.
let g:ale_sign_error = '⤫'
let g:ale_sign_warning = '⚠'

" Enable integration with airline.
let g:airline#extensions#ale#enabled = 1

" Set *.sql files to pgsql syntax highlighting
let g:sql_type_default = 'pgsql'

" Add syntax highlighting for vue react angular jquery lodash d3 etc
let g:used_javascript_libs = 'underscore, angularjs, jquery, d3, vue, react, flux, chai, handlebars'

" Ultisnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

" ignore node_modules x DS_Store x git in ctrlp
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git\|env\|__pycache__'
let g:ctrlp_max_files = 0

" Ignore .pyc files in nerdtree
let NERDTreeIgnore = ['\.pyc$']

" Deoplete settings
let g:deoplete#enable_at_startup = 1

" Deoplete autocompletion settings
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"

" ALE linter options
let g:ale_linters = {
\   'python': ['flake8'],
\}

let g:ale_linters_explicit = 1

"""""""""""""""""""""""""""
"        Misc             "
"                         "
"""""""""""""""""""""""""""

" jedi-vim

let g:jedi#usages_command = "<leader>e"


if exists("b:did_ftplugin")
  finish
endif

runtime! ftplugin/html.vim

" Show syntax highlighting groups for word under cursor
nmap <leader>b :call <SID>SynStack()<CR>
function! <SID>SynStack()
  if !exists("*synstack")
    return
  endif
  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc

function! GenerateTags()
    if &filetype ==# 'python'
        echo 'generating tags'
        :!uctags -R
        echo 'done !'
    endif

endfunc

nnoremap <Leader>t :call GenerateTags()<CR><CR>

""""ag.vim"""
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

cnoreabbrev Ack Ack!
nnoremap <Leader>a :Ack!<Space>