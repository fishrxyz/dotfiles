set __fishr_color_orange FD971F
set __fishr_color_blue 6EC9DD
set __fishr_color_green A6E22E
set __fishr_color_yellow E6DB7E
set __fishr_color_pink F92672
set __fishr_color_grey 554F48
set __fishr_color_white F1F1F1
set __fishr_color_purple 9458FF
set __fishr_color_lilac AE81FF
set __fishr_color_neonpink FF82CF


function __fishr_git_branch_name
  echo (git rev-parse --abbrev-ref HEAD 2> /dev/null)
end

function __fishr_git_status_codes
  echo (git status --porcelain 2> /dev/null | sed -E 's/(^.{3}).*/\1/' | tr -d ' \n')
end

function __fishr_last_exit_code
  set last_exit_code $argv[1]
  # check last status
  if not test $last_exit_code -eq 0
      echo -n ' 📛'
  end
end


function __fishr_kubens
  set_color $__fishr_color_white
  printf ' ['
  set_color $__fishr_color_blue
  printf '🐳 %s'
  echo -n ( kubectx -c)
  set_color $__fishr_color_yellow
  printf ':%s'
  echo -n (kubens -c)
  set_color $__fishr_color_white
  printf ']'
end


function __fishr_git_status
  # we are in a git repo
  if test -n (__fishr_git_branch_name)
    printf ' ('
    set_color $__fishr_color_yellow
    printf '%s' (__fishr_git_branch_name)

    set_color $__fishr_color_white
    echo -n ' -'

    # test for dirty state
    if test -n (__fishr_git_status_codes)
      set_color $__fishr_color_neonpink
      echo -n ' [ダーティ]'
      set_color $__fishr_color_white
      printf ')'
    else
      set_color $fish_color_cwd
      echo -n ' [ينظف]'
      set_color $__fishr_color_white
      printf ')'
    end
  end
end 

function __fishr__aws_info
  printf ' ☁️  %s'
  set_color $__fishr_color_white
  printf 'on'
  echo -n ' '
  set_color $__fishr_color_orange
  printf 'aws'
  echo -n ' '
  set_color $__fishr_color_white
  printf 'in'
  echo -n ' '
  set_color $__fishr_color_neonpink
  echo -n ''
  printf '['
  echo -n $AWS_REGION
  printf ']'
end

function fish_prompt
    set -l last_status $status

    if not set -q VIRTUAL_ENV_DISABLE_PROMPT
        set -g VIRTUAL_ENV_DISABLE_PROMPT true
    end
    set_color $__fishr_color_yellow
    printf '%s' $USER

    set_color $__fishr_color_white
    printf ' at '

    set_color $__fishr_color_neonpink
    echo -n (prompt_hostname)
    set_color normal
    printf ' in '

    set_color $__fishr_color_blue
    printf '%s ' (prompt_pwd)
    set_color normal
    
    __fishr_last_exit_code $last_status

    __fishr_git_status

    __fishr__aws_info

    # Line 2
    echo
    if test -n "$VIRTUAL_ENV"
        printf "(%s) " (set_color blue)(basename $VIRTUAL_ENV)(set_color normal)
    end
    printf '$ '
    set_color normal


end
