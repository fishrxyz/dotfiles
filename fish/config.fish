# # Abbreviations
abbr -a gs  git status
abbr -a gco git checkout
abbr -a gaa git add -A
abbr -a gc  git commit
abbr -a c   clear
abbr -a gl  git log --oneline --color --graph

set -x CAT_EXE $(which bat)

# Aliases
alias python=python3
alias drmi="docker rmi (docker images --filter 'dangling=true' -q --no-trunc)"
alias k="kubecolor"
alias v="$HOME/.local/bin/squashfs-root/AppRun"
alias pods="k get pod"
alias svc="k get svc"
alias ing="k get ingress"
alias kconf="k get conf"
alias ksec="k get secret"
alias kaf="kubectl apply -f"
alias kdl="kubectl delete -f"
alias tf="terraform"
alias vim="nvim"
alias vimcf="cd ~/.dotfiles/nvim && nvim ~/.dotfiles/nvim/init.vim"
alias mkdir="mkdir -vp"
alias dot="cd ~/.dotfiles"
alias dc="docker compose"
alias tmux="tmux -u"
alias activate="source env/bin/activate"
alias ptest="pytest -v -s --disable-warnings"
alias at="alacritty-themes"
alias cat="$CAT_EXE --style plain"
alias copy="xclip -selection clipboard"
alias conf="vim ~/.dotfiles/fish/config.fish"
alias ls="eza"
alias ll="eza -l"
alias la="eza -lah"
alias lad="eza -lahd */"
alias shutdown="shutdown now"
alias restart="shutdown -r"

# Set the path
set -x GOPATH $HOME/gowork
set -x PATH ~/.npm-global/bin $PATH
set -x PATH /usr/local/go/bin $PATH
set -x PATH $GOPATH/bin $PATH
set -x PATH $HOME/.local/bin $PATH
set -x PATH $HOME/.cargo/bin $PATH
set -x PATH /var/lib/snapd/snap/bin $PATH
set -x PATH $HOME/.gem/ruby/2.7.0/bin $PATH
set -x PATH /opt/homebrew/bin $PATH
set -x PATH $HOME/Library/Python/3.9/bin $PATH
set -x PATH $HOME/.local/share/nvim/mason/bin $PATH

# Set the environment variables
set -x EDITOR (which nvim)
set -x TERMINAL alacritty
set -x LANG en_GB.UTF-8
set -x QUOTING_STYLE literal
set -x PYTHON_KEYRING_BACKEND keyring.backends.null.Keyring
set -x AWS_REGION us-east-1
set -x AWS_PROFILE default

if test -f ~/.config/fish/.my_env_vars
    source ~/.config/fish/.my_env_vars
end

# disable fish greeting
function fish_greeting
end

function kbtail
    kubectl logs -f -l app=$argv[1]
end

function _wintermute_create_tmux_session
end

function woo
  set instance_id (aws ec2 describe-instances --region eu-west-3 --filters "Name=tag:Name,Values=Wintermute" --query "Reservations[0].Instances[0].InstanceId" --output text | tr -d '\n')
  if test -n "$argv[1]" -a "$argv[1]" = "down"
    aws ec2 stop-instances --instance-ids	$instance_id --region eu-west-3 | jq
  else
    aws ec2 start-instances --instance-ids	$instance_id --region eu-west-3 | jq
  end
end 

function dev 
  set project_dir $HOME/dev/projects/wintermute-terraform/
  cd $project_dir
  set eip (terraform output -raw -no-color eip)
  ssh -i $project_dir/wintermute.pem fishr@$eip
end 

function clip
    if count $argv > /dev/null
        cat $argv[1] | xclip -selection clipboard
    else
        echo "Copy what foo ??"
    end
end

function poo
    if test (count $argv) -eq 0
        echo "Usage: poo <profile-name>"
        return 1
    end

    set -x AWS_PROFILE $argv[1]

    echo "Switched to AWS profile: $AWS_PROFILE"
end

starship init fish | source
zoxide init --cmd cd fish | source
direnv hook fish | source
