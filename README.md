# How to install fonts

1. Download the fonts from the web
2. Copy them over to `~/.local/share/fonts/` (create the directory if it doesn't exist)
3. Update the font cache with `fc-cache -f -v`

Current dependencies are

- JetBrainsMono Nerd Font Mono
- JetBrainsMono Nerd Font Mono
- Source Han Sans JP
- Source Han Sans KR
- Twemoji

To install, run:

```
yay -S ttf-twemoji adobe-source-han-sans-kr-fonts adobe-source-han-sans-jp-fonts
```

(JetBrainsMono can be downloaded via the nerdfonts website)

## Dependencies for sxhkd / rofi etc

- rofi
- playerctl
- amixer
- nnn
- flameshot
- xorg-xsetroot
- papirus-icon-theme

## Neovim Instructions

After installing neovim, make sure to the following:

- install the plugins by saving the corresponding file (`plugins.lua`)
- install the language servers with Mason
- install the treesitter parsers
