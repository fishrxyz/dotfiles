return {
	"Mofiqul/dracula.nvim",
	name = "dracula",
	priority = 1000,
	config = function()
		local colorscheme = "dracula" -- possible values are dracula and dracula-soft
		vim.cmd.colorscheme("dracula")
	end,
}
