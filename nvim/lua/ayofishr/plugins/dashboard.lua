return {
	"nvimdev/dashboard-nvim",
	event = "VimEnter",
	dependencies = { { "nvim-tree/nvim-web-devicons" } },
	config = function()
		local db = require("dashboard")
		local logo = [[
                                                                             
               ████ ██████           █████      ██                     
              ███████████             █████                             
              █████████ ███████████████████ ███   ███████████   
             █████████  ███    █████████████ █████ ██████████████   
            █████████ ██████████ █████████ █████ █████ ████ █████   
          ███████████ ███    ███ █████████ █████ █████ ████ █████  
         ██████  █████████████████████ ████ █████ █████ ████ ██████ 
      ]]

		logo = string.rep("\n", 8) .. logo .. "\n\n"
		local conf = {}
		conf.header = vim.split(logo, "\n")

		conf.center = {
			{
				icon = "󰈞  ",
				desc = "Find  File",
				action = "Telescope find_files",
				key = "f",
			},
			{
				icon = "󰈢  ",
				desc = "Recently opened files",
				action = "Telescope oldfiles",
				key = "r",
			},
			{
				icon = "󰈬  ",
				desc = "Project grep                            ",
				action = "Telescope live_grep",
				key = "g",
			},
			{
				icon = "  ",
				desc = "New file                                ",
				action = "ene | startinsert",
				key = "n",
			},
			{
				icon = "󰗼  ",
				desc = "Quit Nvim                               ",
				-- desc = "Quit Nvim                               ",
				action = "qa",
				key = "q",
			},
		}

		conf.footer = function()
			local stats = require("lazy").stats()
			local ms = (math.floor(stats.startuptime * 100 + 0.5) / 100)
			return { "⚡ Neovim loaded " .. stats.loaded .. "/" .. stats.count .. " plugins in " .. ms .. "ms" }
		end

		db.setup({
			theme = "doom",
			shortcut_type = "number",
			config = conf,
		})

		vim.api.nvim_create_autocmd("FileType", {
			pattern = "dashboard",
			group = vim.api.nvim_create_augroup("dashboard_enter", { clear = true }),
			callback = function()
				vim.keymap.set("n", "q", ":qa<CR>", { buffer = true, silent = true })
				vim.keymap.set("n", "e", ":enew<CR>", { buffer = true, silent = true })
			end,
		})
	end,
}
