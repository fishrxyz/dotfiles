return {
	"tpope/vim-fugitive",
	config = function()
		vim.keymap.set("n", "<leader>t", vim.cmd.Git, { desc = "Open Fugitive Panel" })
	end,
}
