return {
  "folke/todo-comments.nvim",
  dependencies = { "nvim-lua/plenary.nvim" },
  opts = {
    colors = {
      error = { "#f74762", "DiagnosticError", "ErrorMsg" },
      warning = { "#f98518", "DiagnosticWarn", "WarningMsg" },
      hint = { "#5afcd1", "DiagnosticInfo" },
      info = { "#85dcf7", "DiagnosticHint" },
      default = { "#755afc", "Identifier" },
      test = { "#FF00FF", "Identifier" },
    },
  }
}
