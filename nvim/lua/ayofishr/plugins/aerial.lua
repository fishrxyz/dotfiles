return {
	"stevearc/aerial.nvim",
	opts = {
		placement = "edge",
	},
	dependencies = {
		"nvim-treesitter/nvim-treesitter",
		"nvim-tree/nvim-web-devicons",
	},
	keys = {
		{ "<leader>a", "<cmd>AerialToggle!<CR>", desc = "Toggle the Aerial window" },
	},
	config = true,
}
