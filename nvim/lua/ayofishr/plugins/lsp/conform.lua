return {
	"stevearc/conform.nvim",
  event = { "BufWritePre" },
  cmd = { "ConformInfo" },
	dependencies = { "mason.nvim" },
  opts = {
    format_on_save = { timeout_ms = 500, lsp_fallback = true },
    formatters_by_ft = {
      c = { "clang-format" },
      go = { "goimports", "gofumpt" },
      javascript = { "prettier" },
      typescript = { "prettier" },
      javascriptreact = { "prettier" },
      typescriptreact = { "prettier" },
      json = { "prettier" },
      html = { "prettier" },
      css = { "prettier" },

      lua = { "stylua" },
      markdown = { "markdownlint" },
      python = { "isort", "black" },
    },
  },
  init = function()
    -- If you want the formatexpr, here is the place to set it
    vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"
  end,
}
