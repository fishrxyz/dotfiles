return {
	"williamboman/mason.nvim",
	dependencies = {
		"williamboman/mason-lspconfig.nvim",
		"WhoIsSethDaniel/mason-tool-installer.nvim",
	},
	config = function()
		-- import mason
		local mason = require("mason")

		-- import mason-lspconfig
		local mason_lspconfig = require("mason-lspconfig")

		local mason_tool_installer = require("mason-tool-installer")

		mason.setup({
			ui = {
				icons = {
					package_installed = "✓",
					package_pending = "➜",
					package_uninstalled = "✗",
				},
			},
		})

		mason_lspconfig.setup({
			-- list of servers for mason to install
			ensure_installed = {
				"ansiblels",
				"bashls",
				"clangd",
				"cmake",
				"cssls",
				"cssmodules_ls",
				"dockerls",
				"docker_compose_language_service",
				"emmet_ls",
				"eslint",
				"gopls",
				"grammarly",
				"helm_ls",
				"html",
				"jqls",
				"jsonls",
				"ltex",
				"lua_ls",
				"marksman",
				"pyright",
				"sqlls",
				"tailwindcss",
				"taplo",
				"terraformls",
				"tsserver",
				"vimls",
				"yamlls",
			},
			-- auto-install configured servers (with lspconfig)
			automatic_installation = true, -- not the same as ensure_installed
		})

		mason_tool_installer.setup({
			ensure_installed = {
				"ansible-lint",
				"cfn-lint",
				"cmakelang",
				"eslint_d",
				"flake8",
				"jsonlint",
				"markdownlint",
				"misspell",
				"mypy",
				"yamllint",
				"clang-format",
				"cmakelang",
				"golangci-lint",
				"gofumpt",
				"goimports",
				"markdownlint",
				"prettier",
				"stylua",
				"isort",
				"black",
				"yamlfmt",
			},
		})
	end,
}
