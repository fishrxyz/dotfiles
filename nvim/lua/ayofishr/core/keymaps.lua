-- set leader key to space
vim.g.mapleader = " "
vim.g.maplocalleader = " "

local keymap = vim.keymap
local opts = { noremap = true, silent = true }
local term_opts = { silent = true }

---------------------
-- General Keymaps
---------------------

-- use jk to exit insert mode
keymap.set("i", "jk", "<ESC>", opts)
keymap.set("i", "kj", "<ESC>", opts)

-- " Make Y behave like the other capitals (ie select till the end of the line) DONE
keymap.set("n", "Y", "y$", opts)

-- clear search highlights
keymap.set("n", "<leader>/", ":nohlsearch<cr>", opts)

-- delete single character without copying into register
keymap.set("n", "x", '"_x')

-- source init.lua
keymap.set("n", "<leader>s", ":source $MYVIMRC<cr>", opts)

-- save easily using <space> w DONE
keymap.set("n", "<leader>w", ":w!<cr>", opts)

-- quit easily using <space> q DONE
keymap.set("n", "<leader>q", ":wq!<cr>", opts)

-- Disable this
keymap.set("n", "Q", "<nop>", opts)

-- Easy insertion of a trailing ; or , from insert mode
keymap.set("n", ";;", "A;<Esc>", opts)

-- keep it centered (whatever this is)
keymap.set("n", "n", "nzzzv", opts)
keymap.set("n", "N", "Nzzzv", opts)
keymap.set("n", "J", "mzJ`z", opts)

-- Stay in indent mode (visual mode)
keymap.set("v", "<", "<gv", opts)
keymap.set("v", ">", ">gv", opts)

-- Move text up and down (visual mode)
keymap.set("v", "<A-j>", ":m .+1<CR>==", opts)
keymap.set("v", "<A-k>", ":m .-2<CR>==", opts)
keymap.set("v", "p", '"_dP', opts)

-- Move text up and down (visual block mode)
keymap.set("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap.set("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap.set("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap.set("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)

-- Maintain the cursor position when yanking a visual selection (visual mode)
-- http://ddrscott.github.io/blog/2016/yank-without-jank/
keymap.set("v", "y", "myy`y", opts)
keymap.set("v", "Y", "myY`y", opts)

-- Better terminal navigation
keymap.set("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
keymap.set("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
keymap.set("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
keymap.set("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)

---------------------
-- Window management
---------------------

-- Use comma to switch between tabs
keymap.set("n", ",", "<C-W><C-W>", opts)

-- better window navigation
keymap.set("n", "<C-h>", "<C-w>h", opts)
keymap.set("n", "<C-j>", "<C-w>j", opts)
keymap.set("n", "<C-k>", "<C-w>k", opts)
keymap.set("n", "<C-l>", "<C-w>l", opts)

-- Navigate buffers / tabs
keymap.set("n", "<Tab>", ":tabnext<CR>", opts)
keymap.set("n", "<S-Tab>", ":tabprevious<CR>", opts)

----------------------
-- Plugin Keybinds
----------------------

-- vim-maximizer
keymap.set("n", "<leader>sm", ":MaximizerToggle<CR>") -- toggle split window maximization

-- LSP
keymap.set("n", "<leader>rs", ":LspRestart<CR>")
