local opt = vim.opt

-- line numbers --
opt.number = true -- set numbered lines
opt.relativenumber = true -- set relative numbered lines
opt.numberwidth = 2 -- set number column width to 2 {default 4}

-- tabs & indentation --
opt.tabstop = 2 -- insert 2 spaces for a tab
opt.shiftwidth = 2 -- the number of spaces inserted for each indentation
opt.expandtab = true -- convert tabs to spaces
opt.autoindent = true -- copy indent from current line when starting new one
opt.smartindent = true -- make indenting smarter again
opt.showtabline = 2 -- always show tabs

-- line wrapping --
opt.wrap = false -- disable line wrapping
opt.whichwrap:append("<,>,[,],h,l")
opt.formatoptions:append("cro")

-- search settings --
opt.ignorecase = true -- ignore case when searching
opt.smartcase = true -- if you include mixed case in your search, assumes you want case-sensitive

-- cursor line --
opt.cursorline = true -- highlight the current cursor line

-- ---------- --
-- appearance --
-- ---------- --

-- turn on termguicolors for nightfly colorscheme to work
opt.termguicolors = true
opt.background = "dark" -- colorschemes that can be light or dark will be made dark
opt.signcolumn = "yes" -- show sign column so that text doesn't shift

-- backspace --
opt.backspace = "indent,eol,start" -- allow backspace on indent, end of line or insert mode start position

-- clipboard --
opt.clipboard:append("unnamedplus") -- use system clipboard as default register

-- split windows --
opt.splitright = true -- split vertical window to the right
opt.splitbelow = true -- split horizontal window to the bottom

opt.iskeyword:append("-") -- consider string-string as whole word

-- miscellaneous --

-- backup files --
opt.backup = false -- creates a backup file
opt.swapfile = false -- creates a swapfile
opt.writebackup = false -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited

-- scrolling --
opt.scrolloff = 8 -- is one of my fav
opt.sidescrolloff = 8

-- mouse --
opt.mouse = "a" -- allow the mouse to be used in neovim

-- refresh rate --
opt.timeoutlen = 200 -- time to wait for a mapped sequence to complete (in milliseconds)
opt.updatetime = 300 -- faster completion (4000ms default)
opt.re = 2

-- other random stuff --

opt.cmdheight = 2 -- more space in the neovim command line for displaying messages
opt.completeopt = { "menuone", "noselect" } -- mostly just for cmp
opt.conceallevel = 0 -- so that `` is visible in markdown files
opt.fileencoding = "utf-8" -- the encoding written to a file
opt.hlsearch = true -- highlight all matches on previous search pattern
opt.pumheight = 10 -- pop up menu height
opt.showmode = false -- we don't need to see things like -- INSERT -- anymore
opt.undofile = true -- enable persistent undo
opt.guifont = "monospace:h17" -- the font used in graphical neovim applications

opt.shortmess:append("c")

vim.filetype.add({
	extension = {
		tf = "terraform",
	},
})

-- line numbers --
opt.number = true -- set numbered lines
opt.relativenumber = true -- set relative numbered lines
opt.numberwidth = 2 -- set number column width to 2 {default 4}

-- tabs & indentation --
opt.tabstop = 2 -- insert 2 spaces for a tab
opt.shiftwidth = 2 -- the number of spaces inserted for each indentation
opt.expandtab = true -- convert tabs to spaces
opt.autoindent = true -- copy indent from current line when starting new one
opt.smartindent = true -- make indenting smarter again
opt.showtabline = 2 -- always show tabs

-- line wrapping --
opt.wrap = false -- disable line wrapping
opt.whichwrap:append("<,>,[,],h,l")
opt.formatoptions:append("cro")

-- search settings --
opt.ignorecase = true -- ignore case when searching
opt.smartcase = true -- if you include mixed case in your search, assumes you want case-sensitive

-- cursor line --
opt.cursorline = true -- highlight the current cursor line

-- ---------- --
-- appearance --
-- ---------- --

-- turn on termguicolors for nightfly colorscheme to work
opt.termguicolors = true
opt.background = "dark" -- colorschemes that can be light or dark will be made dark
opt.signcolumn = "yes" -- show sign column so that text doesn't shift

-- backspace --
opt.backspace = "indent,eol,start" -- allow backspace on indent, end of line or insert mode start position

-- clipboard --
opt.clipboard:append("unnamedplus") -- use system clipboard as default register

-- split windows --
opt.splitright = true -- split vertical window to the right
opt.splitbelow = true -- split horizontal window to the bottom

opt.iskeyword:append("-") -- consider string-string as whole word

-- miscellaneous --

-- backup files --
opt.backup = false -- creates a backup file
opt.swapfile = false -- creates a swapfile
opt.writebackup = false -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited

-- scrolling --
opt.scrolloff = 8 -- is one of my fav
opt.sidescrolloff = 8

-- mouse --
opt.mouse = "a" -- allow the mouse to be used in neovim

-- refresh rate --
opt.timeoutlen = 200 -- time to wait for a mapped sequence to complete (in milliseconds)
opt.updatetime = 300 -- faster completion (4000ms default)
opt.re = 2

-- other random stuff --

opt.cmdheight = 2 -- more space in the neovim command line for displaying messages
opt.completeopt = { "menuone", "noselect" } -- mostly just for cmp
opt.conceallevel = 0 -- so that `` is visible in markdown files
opt.encoding = "utf-8" -- fix terraform lsp nonsense
opt.fileencoding = "utf-8" -- the encoding written to a file
opt.hlsearch = true -- highlight all matches on previous search pattern
opt.pumheight = 10 -- pop up menu height
opt.showmode = false -- we don't need to see things like -- INSERT -- anymore
opt.undofile = true -- enable persistent undo
opt.guifont = "monospace:h17" -- the font used in graphical neovim applications

opt.shortmess:append("c")

opt.virtualedit = "block" -- allows for
opt.inccommand = "split" -- split the window when running text substitution

-- Add terraform filetype
vim.filetype.add({
	extension = {
		tf = "terraform",
	},
})

-- Set yabairc filetype
vim.cmd(
	[[
  augroup YabaiRCFileType
    autocmd!
    autocmd BufNewFile,BufRead properties,yabairc,skhdrc setfiletype sh
  augroup END
]],
	false
)

vim.cmd(
	[[
  augroup TerraformTemplateFileType
    autocmd!
    autocmd BufNewFile,BufRead *.sh.tpl setfiletype sh
    autocmd BufNewFile,BufRead *.json.tpl setfiletype json
    autocmd BufNewFile,BufRead *.yaml.tpl setfiletype yaml
  augroup END
]],
	false
)

vim.cmd(
	[[
  augroup EnvRCFileType
    autocmd!
    autocmd BufNewFile,BufRead .envrc, .env setfiletype sh
  augroup END
]],
	false
)

vim.loader.enable()
