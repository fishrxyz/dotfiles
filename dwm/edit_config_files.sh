#!/bin/bash
output=$(ls ~/.dotfiles | dmenu )
if [ $? == 0 ]; then
    config_file_path=~/.dotfiles/$output
    [ -f  "$config_file_path" ] && alacritty -e nvim $config_file_path || exit 1
else
    exit 1
fi
