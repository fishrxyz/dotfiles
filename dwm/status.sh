#!/bin/bash

while true; do

    AC_PLUGGED=$(cat /sys/class/power_supply/AC/online)
    BATTERY_LEVEL=$(cat /sys/class/power_supply/BAT1/capacity)
    KEYBOARD_BATTERY_DIR=/sys/class/power_supply/hid-dc:2c:26:df:6e:10-battery

    SSID=$(iwgetid -r)

    # volume control
    ALSA_INFO=($(awk -F"[][]" '/Front Left/ { print $2 " " $4 }' <(amixer -D pulse get Master)))
    VOLUME="${ALSA_INFO[0]}"
    MUTED="${ALSA_INFO[1]}"

    if [ $MUTED == "off" ]; then
        VOLUME_INFO="🔇 0%"
    else
        VOLUME_INFO="🔊 $VOLUME"
    fi

    if [ -d "$KEYBOARD_BATTERY_DIR" ]; then
        KEYBOARD_BATTERY_LEVEL=$(cat $KEYBOARD_BATTERY_DIR/capacity)
        KEYBOARD_INFO="⌨ $KEYBOARD_BATTERY_LEVEL%"
    fi

    if [ -z "$SSID" ]; then
        SSID="Not Connected"
    fi

    if [ $AC_PLUGGED == "1" ]; then
        ICON="🔌"
    else
        ICON="🔋"
    fi

	xsetroot -name "🕐 $(date +"%H:%M") | $VOLUME_INFO | 🌍 $SSID | $KEYBOARD_INFO | $ICON$BATTERY_LEVEL% "

	sleep 0.5
done
