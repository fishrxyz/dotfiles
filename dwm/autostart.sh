#! /bin/bash

# start dunst
dunst &

# Configure external display(s)
MONITOR_SCRIPT="/home/$USER/.dwm/monitor_switcher.sh"
("$MONITOR_SCRIPT")

# Set wallpaper
feh --bg-scale --randomize /home/zabana/Pictures/*

# start status bar script
STATUS_SCRIPT="/home/$USER/.dwm/status.sh"
("$STATUS_SCRIPT" &)

#start brave
brave &
